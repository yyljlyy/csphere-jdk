# csphere-jdk基于csphere/alpine镜像构建

**如何使用该镜像**
```
docker pull csphere/jdk:8
```
**快速开始**
```
docker run -it csphere/jdk:8 sh
```
**执行构建命令**
```
docker build -t csphere/jdk .
```
##用例测试

> **创建本地Java工作路径**
> ```
> mkdir /myapp
> ```
> **创建java文件**
> ```
> echo -e 'public class Test { public static void main(String[] args) { System.out.println("Hello World"); } }'>Test.java
> ```
> **测试用例**
> ```
> docker run --rm -v `pwd`:/myapp --workdir /myapp csphere/jdk:8 sh -c 'javac Test.java && java Test'
> ```